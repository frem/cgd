﻿using UnityEngine;
using System.Collections;

public class Dying : MonoBehaviour
{
	// Only sound
	// volume of dying cell (0.1f - 1.0f)
	float maxSoundForDying = 0.2f;

	// Update is called once per frame
	void Update ()
	{
		if (PlayerInfo.playerInfo.Dead) {
			GetComponent<AudioSource>().loop = false;
			return;
		}

		if (PlayerInfo.playerInfo.Dying) {
			GetComponent<AudioSource>().volume = Mathf.Lerp (GetComponent<AudioSource>().volume, maxSoundForDying, Time.deltaTime * AudioCenter.SoundFadeSpeed);
		} else {
			GetComponent<AudioSource>().volume = Mathf.Lerp (GetComponent<AudioSource>().volume, 0f, Time.deltaTime * AudioCenter.SoundFadeSpeed);
		}
		
		if (GetComponent<AudioSource>().volume < 0.1f) {
			GetComponent<AudioSource>().Pause ();
			
		} else if (!GetComponent<AudioSource>().isPlaying) {
			GetComponent<AudioSource>().Play ();
		}
	}
}
