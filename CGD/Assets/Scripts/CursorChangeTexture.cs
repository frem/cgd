﻿using UnityEngine;
using System.Collections;

public class CursorChangeTexture : MonoBehaviour
{
	SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Awake ()
	{
		spriteRenderer = transform.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButton (0) && transform.GetComponent<Renderer>().enabled) {
			GameObject[] cells = GameObject.FindGameObjectsWithTag ("cell") as GameObject[];
			//Debug.Log(cells);
			bool tooClose = false;
			// get distance of cursor from cells
			foreach (GameObject cell in cells) {
				float distance = Vector3.Distance (cell.transform.position, transform.position);
				// localScale - constant
				if (distance < (2f * cell.transform.localScale.x)) {
					tooClose = true;
				}
				//Debug.Log(distance);
			}
			// set color of cursor during cell creation
			if (tooClose) {
				spriteRenderer.color = Color.red;
			} else {
				spriteRenderer.color = Color.white;
			}
		}
	}
}
