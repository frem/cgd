﻿using UnityEngine;
using System.Collections;

public class CellHit : MonoBehaviour
{
	static float MinMagnitudeToPlaySound = 5f;

	void OnTriggerStay2D (Collider2D coll)
	{
		if ((coll.gameObject.tag != "cell") || (coll.gameObject.tag != "level")) {
			return;
		}

		if (gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > MinMagnitudeToPlaySound) {
			AudioCenter.audioCenter.Play (AudioCenter.audioCenter.CellHitClip);
		}
	}
}
