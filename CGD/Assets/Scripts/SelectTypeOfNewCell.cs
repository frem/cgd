using UnityEngine;
using System.Collections;

public class SelectTypeOfNewCell : MonoBehaviour
{
	public GameObject BuildingBackground;

	public GameObject MainCell;
	public Texture2D MainCellTexture;
	public bool MainShownInBuilding = true;
	public GameObject EmptyCell;
	public Texture2D EmptyCellTexture;
	public bool EmptyShownInBuilding = true;
	public GameObject O2Cell;
	public Texture2D O2CellTexture;
	public bool O2ShownInBuilding = true;
	public GameObject EatingCell;
	public Texture2D EatingCellTexture;
	public bool EatingShownInBuilding = true;
	public GameObject StoreCell;
	public Texture2D StoreCellTexture;
	public bool StoreShownInBuilding = true;
	public GameObject MovingCell;
	public Texture2D MovingCellTexture;
	public bool MovingShownInBuilding = true;
	public Texture2D RemoveCellTexture;
	public bool RemoveShownInBuilding = true;

	public string InfoHelpBuildingMode;
	public string InfoHelpNotBuildingMode;

	Color highlightedButton = Color.yellow;

	// singleton
	static SelectTypeOfNewCell _typeCell;
	public static SelectTypeOfNewCell typeCell {
		get { return _typeCell;}
	}

	void Start ()
	{
		PlayerInfo.playerInfo.NewCellType = EmptyCell;

		// set text in level
		InfoHelpNotBuildingMode = Help.help.SetHelpTextNotBuildingMode (Application.loadedLevelName);
		InfoHelpBuildingMode = Help.help.SetHelpTextBuildingMode (Application.loadedLevelName);
	}

	void Awake ()
	{
		// singleton
		_typeCell = this;
	}

	void OnGUI ()
	{
		if (Time.timeScale == 0f) {
			return;
		}

		// if you are in building area you can build
		if (PlayerInfo.playerInfo.IsInBuildingArea || PlayerInfo.playerInfo.BuildingMode) {
			string buttonText = PlayerInfo.playerInfo.BuildingMode ? "RESUME" : "BUILD";
			if (GUI.Button (new Rect (20f, 20f, 100f, 40f), buttonText)) {
				PlayerInfo.playerInfo.BuildingMode = !PlayerInfo.playerInfo.BuildingMode;	
			}
		}

		if (BuildingBackground) {
			(BuildingBackground.GetComponent<SpriteRenderer> ()).enabled = PlayerInfo.playerInfo.BuildingMode;
		}

		if (!PlayerInfo.playerInfo.BuildingMode) {
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (20f, 70f, 200f, 200f), InfoHelpNotBuildingMode);
			}
			// show only if it is in pause mode
			return;
		}

		GUI.skin.button.alignment = TextAnchor.MiddleLeft;
		float y = 70f;

		if (MainShownInBuilding) {
			createButton (y, MainCell, MainCellTexture, PlayerInfo.playerInfo.NumMain, "Main");
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (130f, y + 10f, 200f, 40f), "Main Cell - Only for development purposes.");
			}
			y += 40f;
		}

		if (EmptyShownInBuilding) {
			createButton (y, EmptyCell, EmptyCellTexture, PlayerInfo.playerInfo.NumEmpty, "Empty");
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (130f, y + 10f, 200f, 40f), "Empty Cell does nothing.");
			}
			y += 40f;
		}

		if (O2ShownInBuilding) {
			createButton (y, O2Cell, O2CellTexture, PlayerInfo.playerInfo.NumO2, "O2");
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (130f, y + 10f, 200f, 40f), "O2 Cell helps you defy gravity.");
			}
			y += 40f;
		}

		if (EatingShownInBuilding) {
			createButton (y, EatingCell, EatingCellTexture, PlayerInfo.playerInfo.NumEating, "Eating");
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (130f, y + 10f, 200f, 40f), "Eating Cell gains energy.");
			}
			y += 40f;
		}

		if (StoreShownInBuilding) {
			createButton (y, StoreCell, StoreCellTexture, PlayerInfo.playerInfo.NumStore, "Store");
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (130f, y + 10f, 200f, 40f), "Store Cell stores energy.");
			}
			y += 40f;
		}

		if (MovingShownInBuilding) {
			createButton (y, MovingCell, MovingCellTexture, PlayerInfo.playerInfo.NumMoving, "Moving");
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (130f, y, 200f, 40f), "Moving Cell gives you speed but spends energy.");
			}
			y += 40f;
		}

		if (RemoveShownInBuilding) {
			y += 10f;
			if (PlayerInfo.playerInfo.NewCellType == null) {
				GUI.color = highlightedButton;
			} else {
				GUI.color = Color.white; // default
			}
			GUI.enabled = true;
			if (GUI.Button (new Rect (20f, y, 100f, 40f), new GUIContent ("Remove", RemoveCellTexture))) {
				PlayerInfo.playerInfo.NewCellType = null;
			}
			if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
				GUI.Label (new Rect (130f, y, 200f, 40f), "Remove the cells you have created.");
			}
			y += 40;
		}

		GUI.skin.button.alignment = TextAnchor.MiddleCenter; // default
		GUI.color = Color.white; // default

		if (PlayerInfo.playerInfo.InfoHelp) { // displays only if help is activated
			y += 10f;
			GUI.Label (new Rect (20f, y, 200f, 200f), InfoHelpBuildingMode);
		}
	}

	void createButton (float y, GameObject cell, Texture2D cellTexture, int number, string name)
	{
		if (PlayerInfo.playerInfo.NewCellType == cell) {
			GUI.color = highlightedButton;
		} else {
			GUI.color = Color.white; // default
		}
		GUI.enabled = (number > 0);
		if (GUI.Button (new Rect (20f, y, 100f, 40f), new GUIContent (name, cellTexture))) {
			PlayerInfo.playerInfo.NewCellType = cell;
		}
		GUI.skin.label.alignment = TextAnchor.UpperRight;
		GUI.Label (new Rect (20f, y, 95f, 30f), number.ToString ());
		GUI.skin.label.alignment = TextAnchor.UpperLeft;
	}
}
