﻿using UnityEngine;
using System.Collections;

public class LevelPlanet : MonoBehaviour
{
	public string LevelName;
	Color basic = Color.white;
	Color pressed = Color.blue;

	// planet texture
	SpriteRenderer spriteRenderer;

	// planet effect
	SpriteRenderer spriteRendererEffect;

	// planet text
	SpriteRenderer spriteRendererText;

	// mouse
	bool buttonDown = false;

	void Awake ()
	{
		spriteRenderer = transform.GetComponent<SpriteRenderer> ();
		spriteRendererEffect = transform.FindChild ("LevelEffect").GetComponent<SpriteRenderer> ();
		spriteRendererText = transform.FindChild ("LevelText").GetComponent<SpriteRenderer> ();
		basic = spriteRenderer.color;
	}

	void OnMouseEnter ()
	{
		spriteRendererEffect.enabled = true;
		spriteRendererText.enabled = true;
		buttonDown = false;
		LevelCamera.levelCamera.OverLevel = true;
	}

	void OnMouseExit ()
	{
		spriteRenderer.color = basic;
		spriteRendererEffect.enabled = false;
		spriteRendererText.enabled = false;
		buttonDown = false;
		LevelCamera.levelCamera.OverLevel = false;
	}

	void OnMouseOver ()
	{
		// left mouse button is pressed when going over object
		if (Input.GetMouseButton (0)) {
			spriteRenderer.color = pressed;
			buttonDown = true;
		} else {
			spriteRenderer.color = basic;
			if (buttonDown) {
				MusicMenu.musicMenu.StopMusic ();
				Application.LoadLevel (LevelName);
			}
		}
	}
}
