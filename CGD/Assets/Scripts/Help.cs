﻿using UnityEngine;
using System.Collections;

public class Help : MonoBehaviour
{
	// singleton
	static Help _help;
	public static Help help {
		get { return _help;}
	}

	// Use this for initialization
	void Awake ()
	{
		// singleton
		_help = this;
	}

	public string SetHelpTextNotBuildingMode (string levelName)
	{
		if (levelName == "Tutorial1") {
			return "Hi, welcome to The Cell game. The cell goes where you click or hold left mouse button. Try it ! In every level your mission is to find the finish (purple area).";
		}
		if (levelName == "Tutorial2") {
			return "In this tutorial, the BUILD button is introduced. You can use it in the white areas only. Try create an organism. Be creative!";
		}
		if (levelName == "Tutorial3") {
			return "Now you start with the organism. It is heavy. Try build some new O2 cells. Maybe they will help you.";
		}
		if (levelName == "Tutorial4") {
			return "You need energy to survive. To gain energy you need Eating cells. There are two types of energy source Particles and Resources.";
		}
		if (levelName == "Tutorial5") {
			return "The same level as previous one but with less Resources. Store cell can be your solution.";
		}
		if (levelName == "Tutorial6") {
			return "Check build. Also you can push blocking rocks out of tunnels.";
		}
		if (levelName == "Tutorial7") {
			return "On half way to finish you will find second building area. Try new option Removing.";
		}
		if (levelName == "Tutorial8") {
			return "Last tutorial. Last thing you should be familiar with. There are traps, which can injure or even destroy your organism. It also drains its energy. So don't stay in the red areas too long and and try to avoid them. Good luck!";
		}
		if ((levelName == "Level1-1") ||
			(levelName == "Level1-2") ||
			(levelName == "Level2-1") ||
			(levelName == "Level2-2")) {

			return "If you need help play some tutorials first.";
		}
		return "";
	}

	public string SetHelpTextBuildingMode (string levelName)
	{
		if (levelName == "Tutorial1") {
			return "";
		}
		if (levelName == "Tutorial2") {
			return "Drag from existing cell in some direction. If the cursor is white, the cell will be created. If the cursor is red, you need to find another place for the new cell. When you are done, click the RESUME button to continue the game.";
		}
		if (levelName == "Tutorial3") {
			return "Button of choosen cell type is yellow.";
		}
		if (levelName == "Tutorial4") {
			return "";
		}
		if (levelName == "Tutorial5") {
			return "Hurray, you can use Store Cell now! It can help you with energy problems. Build it and watch what happens with the energy bar at the bottom. Do you see new bars which appear when the cells are built? Be careful, Store Cells are heavy! Try to balance the weight with O2 Cells.";
		}
		if (levelName == "Tutorial6") {
			return "Let us introduce you the last type of cell - the Moving Cell. Use it carefully, it needs much more energy than the Main Cell.";
		}
		if (levelName == "Tutorial7") {
			return "You can remove only the cells you have created.";
		}
		if (levelName == "Tutorial8") {
			return "";
		}
		if ((levelName == "Level1-1") ||
			(levelName == "Level1-2") ||
			(levelName == "Level2-1") ||
			(levelName == "Level2-2")) {

			return "If you need help play some tutorials first.";
		}
		return "";
	}
}
