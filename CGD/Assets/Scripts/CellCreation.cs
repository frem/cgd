using UnityEngine;
using System.Collections;

public class CellCreation : MonoBehaviour
{
	public bool IsCellConnected;
	public bool CanBeRemoved = true;
	public bool WasRemoved = false;
	Color cannnotBeRemovedColor = new Color (1f, 1f, 1f, 0.5f);

	/** variable for cell distance */
	public float Distance = 2f;

	GameObject cursor;
	bool dragging;
	float damping;
	float frequency;
	float maximumDistance;
	float minimumDistance;

	// Use this for initialization
	void Start ()
	{
		damping = 1f;
		frequency = 0f;
		maximumDistance = Distance + 0.5f;
		// 2 * radius
		minimumDistance = 2f * transform.localScale.x;

		dragging = false;
	}

	void Awake ()
	{
		cursor = GameObject.FindGameObjectWithTag ("cursor");
	}

	void Update ()
	{
		if ((gameObject.tag == "cell") && PlayerInfo.playerInfo.BuildingMode) {
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			GetComponent<Rigidbody2D>().angularVelocity = 0f;
		}
	}

	void LateUpdate ()
	{
		if (PlayerInfo.playerInfo.Finish) {
			GetComponent<Rigidbody2D>().velocity = Vector2.Lerp (GetComponent<Rigidbody2D>().velocity, Vector2.zero, Time.deltaTime);
			GetComponent<Rigidbody2D>().angularVelocity = Mathf.Lerp (GetComponent<Rigidbody2D>().angularVelocity, 0f, Time.deltaTime);
		}

		// in building mode disable halo
		Behaviour h = (Behaviour)transform.FindChild ("Halo").GetComponent ("Halo");
		if (h && PlayerInfo.playerInfo.BuildingMode) {
			h.enabled = false;
		} else {
			h.enabled = true;
		}

		// disable/enable cell when remove is selected
		if ((PlayerInfo.playerInfo.NewCellType == null) &&
			PlayerInfo.playerInfo.BuildingMode &&
			(!CanBeRemoved)) {

			transform.FindChild ("cell").GetComponent<SpriteRenderer> ().color = cannnotBeRemovedColor;
		} else {
			transform.FindChild ("cell").GetComponent<SpriteRenderer> ().color = Color.white;
		}
	}

	void OnMouseDown ()
	{
		if (!PlayerInfo.playerInfo.CanBuildCell ()) {
			return;
		}

		setCursorMousePosition ();

		if ((!cursor.GetComponent<Renderer>().enabled) && PlayerInfo.playerInfo.BuildingMode) {
			cursor.GetComponent<Renderer>().enabled = true;
		
			dragging = true;
		}
	}

	void OnMouseDrag ()
	{
		if (dragging) {
			setCursorMousePosition ();
		}
	}

	void OnMouseUp ()
	{
		cursor.GetComponent<Renderer>().enabled = false;

		// if you drag then create new cell
		if (dragging) {
			dragging = false;

			createNewCell ();
		}
	}

	// you can create new cells (build) only if you are in building area
	void OnTriggerStay2D (Collider2D coll)
	{
		if (coll.gameObject.tag == "buildingArea") {
			PlayerInfo.playerInfo.IsInBuildingArea = true;
		}
	}

	/**
	 * Sets Cursor for dragging in direction of mouse in setted boundaries
	 */
	void setCursorMousePosition ()
	{
		Plane plane = new Plane (Vector3.forward, transform.position);
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		float hitDistance = 0f;
		if (plane.Raycast (ray, out hitDistance)) {
			// point on screen with mouse cursor
			Vector3 point = ray.GetPoint (hitDistance);
			// point of original cell
			Vector3 center = transform.position;
			
			// distance in minimum - maximum distance
			float actualDistance = Vector3.Distance (center, point);
			float t = actualDistance;
			if (actualDistance > (maximumDistance - 0.1f)) {
				t = maximumDistance - 0.1f;
			} else {
				if (actualDistance < (minimumDistance + 0.1f)) {
					t = minimumDistance + 0.1f;
				}
			}
			
			// new point in direction of mouse
			Vector3 directionVector = point - center;
			Vector3 newPoint = center + (t * directionVector.normalized);
			// new cell cursor position where you drag
			cursor.transform.position = newPoint;
		}
	}

	/**
	 * Creates new cell on position setted by mouse cursor (if not too close to other cells)
	 * and connects it with other cell in distance (mininumDistance, 2 * maximumDistance)
	 */
	void createNewCell ()
	{
		GameObject[] allObjects = GameObject.FindGameObjectsWithTag ("cell") as GameObject[];
		float minDistance = float.MaxValue;
		ArrayList cellsToConnect = new ArrayList ();

		// search through objects
		foreach (GameObject cell in allObjects) {
			// finds min distance
			float actualDistance = Vector3.Distance (cell.transform.position, cursor.transform.position);
			if (actualDistance < minDistance) {
				minDistance = actualDistance;
			}

			// add cells to connect, if in correct distance
			if ((actualDistance >= minimumDistance) && (actualDistance <= (1.7f * maximumDistance))) {
				cellsToConnect.Add (cell);
			}
		}

		// some cell is too close or no cell to connect
		if ((minDistance < minimumDistance) || (cellsToConnect.Count == 0))
			return;

		// create new object from old one with selected type
		GameObject cloneFrom = PlayerInfo.playerInfo.NewCellType;

		if (cloneFrom == null) {
			cloneFrom = gameObject;
		}

		GameObject clone = Instantiate (cloneFrom, Vector3.zero, Quaternion.identity) as GameObject;
		clone.transform.position = cursor.transform.position;
		clone.transform.parent = transform.parent;

		setNewCellName (clone);
		PlayerInfo.playerInfo.UpdateNumberOfCell (clone, -1);

		// delete old springjoints
		SpringJoint2D[] joints = clone.GetComponents<SpringJoint2D> ();
		foreach (SpringJoint2D joint in joints) {
			Destroy (joint);
		}

		foreach (GameObject cell in cellsToConnect) {
			// add new springjoint
			SpringJoint2D springJoint = clone.AddComponent<SpringJoint2D> ();
			springJoint.connectedBody = cell.GetComponent<Rigidbody2D>();
			springJoint.distance = Vector3.Distance (cell.transform.position, clone.transform.position);
			springJoint.dampingRatio = damping;
			springJoint.frequency = frequency;

			// opposite direction
			springJoint = cell.AddComponent<SpringJoint2D> ();
			springJoint.connectedBody = clone.GetComponent<Rigidbody2D>();
			springJoint.distance = Vector3.Distance (cell.transform.position, clone.transform.position);
			springJoint.dampingRatio = damping;
			springJoint.frequency = frequency;
		}

		// play sound
		AudioCenter.audioCenter.PlayCellCreated ();

		PlayerInfo.playerInfo.NeedCheckConnection = true;
	}

	void setNewCellName (GameObject newCell)
	{
		if (PlayerInfo.playerInfo.NewCellType == SelectTypeOfNewCell.typeCell.MainCell) {
			newCell.name = "mainCell";
		}
		if (PlayerInfo.playerInfo.NewCellType == SelectTypeOfNewCell.typeCell.EmptyCell) {
			newCell.name = "emptyCell";
		}
		if (PlayerInfo.playerInfo.NewCellType == SelectTypeOfNewCell.typeCell.EatingCell) {
			newCell.name = "eatingCell";
		}
		if (PlayerInfo.playerInfo.NewCellType == SelectTypeOfNewCell.typeCell.StoreCell) {
			newCell.name = "storeCell";
		}
		if (PlayerInfo.playerInfo.NewCellType == SelectTypeOfNewCell.typeCell.MovingCell) {
			newCell.name = "movingCell";
		}
		if (PlayerInfo.playerInfo.NewCellType == SelectTypeOfNewCell.typeCell.O2Cell) {
			newCell.name = "o2Cell";
		}
	}
}
