﻿using UnityEngine;
using System.Collections;

public class Traps : MonoBehaviour
{
	// damage to cell health (and energy to maincell)
	public static float DamagePerSecond = 50f;

	bool cellInTrap;
		
	void FixedUpdate ()
	{
		cellInTrap = false;		
	}

	void OnTriggerStay2D (Collider2D coll)
	{
		if (coll.gameObject.tag == "cell") {
			cellInTrap = true;
		}
	}

	void LateUpdate ()
	{
		if (PlayerInfo.playerInfo.Finished || PlayerInfo.playerInfo.Dead) {
			GetComponent<AudioSource>().volume = Mathf.Lerp (GetComponent<AudioSource>().volume, 0f, Time.deltaTime * AudioCenter.SoundFadeSpeed);
			return;
		}

		if (cellInTrap) {
			GetComponent<AudioSource>().volume = Mathf.Lerp (GetComponent<AudioSource>().volume, 1f, Time.deltaTime * AudioCenter.SoundFadeSpeed);
		} else {
			GetComponent<AudioSource>().volume = Mathf.Lerp (GetComponent<AudioSource>().volume, 0f, Time.deltaTime * AudioCenter.SoundFadeSpeed);
		}

		if (GetComponent<AudioSource>().volume < 0.1f) {
			GetComponent<AudioSource>().Pause ();

		} else {
			if (!GetComponent<AudioSource>().isPlaying) {
				GetComponent<AudioSource>().Play ();
			}
		}
	}
}
