﻿using UnityEngine;
using System.Collections;

public class AudioCenter : MonoBehaviour
{
	public const float SoundFadeSpeed = 5f;

	public AudioClip CellHitClip;
	public AudioClip CellCreatedClip;
	public AudioClip CellDestroyedClip;
	public AudioClip CellDeletedClip;

	public AudioClip WinClip;
	public AudioClip FailClip;

	// singleton
	static AudioCenter _audioCenter;
	public static AudioCenter audioCenter {
		get { return _audioCenter; }
	}

	void Awake ()
	{
		// singleton
		_audioCenter = this;
	}

	public void Play (AudioClip audioClip)
	{
		GetComponent<AudioSource>().PlayOneShot (audioClip, 1f);
	}
	
	public void PlayCellCreated ()
	{
		Play (CellCreatedClip);
	}

	public void PlayCellDestroyed ()
	{
		Play (CellDestroyedClip);
	}

	public void PlayCellDeleted ()
	{
		Play (CellDeletedClip);
	}

	public void PlayWin ()
	{
		Play (WinClip);
	}
}
