﻿using UnityEngine;
using System.Collections;

public class CellDebug : MonoBehaviour
{
	// Update is called once per frame
	void LateUpdate ()
	{
		// Joints
		SpringJoint2D[] joints = gameObject.GetComponents<SpringJoint2D> ();
		foreach (SpringJoint2D joint in joints) {
			Debug.DrawLine (transform.position,
			                joint.connectedBody.GetComponent<Rigidbody2D>().gameObject.transform.position,
			                Color.red);
		}
	}
}
