using UnityEngine;
using System.Collections;

public class PlayerInfo : MonoBehaviour
{
	public static float Volume = 1f;
	public static bool Info = false;

	public static float CellEnergyStoreCapacity = 100f;

	// should be smaller than DamagePerSecond
	public static float CellDyingEnergyConsume = 30f;
	public static float HealingPerSecond = 10f;
	public static float DamageIfNotConnected = 30f;

	public GameObject Organism;
	public int NumMain = 1;
	public int NumEmpty = 20;
	public int NumO2 = 10;
	public int NumMoving = 10;
	public int NumEating = 10;
	public int NumStore = 10;

	// starting energy
	public float Energy = 80f;
	// maximal energy
	public float MaxEnergy = 100f;

	public string NextLevel;

	// update time for cells dead script
	public float UpdateTime = 0.5f;
	// boolean variable for checking connection to main cell
	public bool NeedCheckConnection = false;

	public bool IsInTrap;

	public bool IsInBuildingArea;
	public bool BuildingMode;
	public bool Finish;

	public bool Dying {
		get { return (Energy <= 0f); }
	}

	public bool Dead {
		get { return (Energy <= (- CellEnergyStoreCapacity)); }
	}

	public bool Finished {
		get { return (finishingTime <= 0f); }
	}

	public bool InfoHelp = false;

	// pointer on cell in building mode
	public GameObject NewCellType;
	// rectangle for camera position
	public Vector2 CellsRectMax, CellsRectMin;

	// structure for algoritm in cells dead script
	struct CellsConnected
	{
		public GameObject cell;
		public bool isConnected;
		public bool isChecked;
	}
	// variable where is every cell
	CellsConnected[] cellsConnected;
	int mainCellIndex;
	// time variable for running cell connection algoritms (only once a while)
	float nextUpdate = 0f;

	// time in second in finish area
	float finishingTime = 2f;

	// if true the level will started in building mode
	bool initialBuildMode = true;

	// shortcut for cursor
	GameObject _cursor;
	public static GameObject cursor {
		get { return PlayerInfo.playerInfo._cursor;}
	}

	// singleton
	static PlayerInfo _playerInfo;
	public static PlayerInfo playerInfo {
		get { return _playerInfo;}
	}

	// -------------------------- FUNCTIONS -----------------------------

	void Start ()
	{
		Finish = false;

		InfoHelp = PlayerInfo.Info;

		if (Application.loadedLevelName.Substring (0, 8) == "Tutorial") {
			InfoHelp = true;
			Info = true;
		}
	}

	// Use this for initialization
	void Awake ()
	{
		// set time
		Time.timeScale = 1.0f;

		// get cursor for cell creation
		_cursor = GameObject.FindGameObjectWithTag ("newCellCursor");

		// singleton
		_playerInfo = this;

		// create start organism
		if (Organism != null) {
			GameObject org = Instantiate (Organism, Vector3.zero, Quaternion.identity) as GameObject;	
			org.transform.parent = transform;
		}
	}
	
	void FixedUpdate ()
	{
		IsInBuildingArea = false;
		IsInTrap = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (initialBuildMode && PlayerInfo.playerInfo.IsInBuildingArea) {
			PlayerInfo.playerInfo.BuildingMode = true;
			initialBuildMode = false;
		}

		// check what cells are not connected max once in updateTime
		if (NeedCheckConnection && (!BuildingMode)) { // game is not in build mode
			float timeOfDead = Time.time;
			if (timeOfDead >= nextUpdate) {
				nextUpdate = timeOfDead + UpdateTime;
				NeedCheckConnection = false;
				//script
				fillUpStructure ();
				checkCell (mainCellIndex); // algoritm
				killNotConnectedCells (); // remove not connected cells
			}
		}

		// cell is dying
		if (Energy <= 0f) {
			Energy -= CellDyingEnergyConsume * Time.deltaTime;
		}

		// finishing
		if (Finish && (finishingTime > 0f)) {
			Energy = Mathf.Max (Energy, 1f); // not dying in finish
			finishingTime -= Time.deltaTime; // countdown
		}
	}

	void LateUpdate ()
	{
		// Energy in restrictions
		if (Energy > MaxEnergy) {
			Energy = MaxEnergy;
		}

		if (Energy <= (-CellEnergyStoreCapacity)) {
			Energy = (-CellEnergyStoreCapacity);
		}
	}

	// ----------------- FUNCTIONS FOR CHECK CELL CONNECTIONS -----------------------------

	void fillUpStructure ()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag ("cell") as GameObject[];
		cellsConnected = new CellsConnected[cells.Length]; // array with boolean value
		int i = 0; // help variable for filling cellsConnected
		foreach (GameObject cell in cells) {
			cellsConnected [i].cell = cell; // fill cellsConnected
			cellsConnected [i].isChecked = false; // initialization
			cellsConnected [i].isConnected = false; // initialization
			if (cell.gameObject.name == "MainCell") {
				mainCellIndex = i;
			}
			i++;
		}
	}

	
	void checkCell (int index)
	{
		if (cellsConnected [index].isChecked) {
			return;
		}

		cellsConnected [index].isChecked = true;
		cellsConnected [index].isConnected = true;

		// go through all the springjoints
		SpringJoint2D[] cellJoints = cellsConnected [index].cell.GetComponents<SpringJoint2D> ();
		foreach (SpringJoint2D joint in cellJoints) {
			// find cell to which is checking cell connected
			GameObject secondCell = joint.connectedBody.GetComponent<Rigidbody2D>().gameObject;

			// searching through cells in cellsConnected for cell found in up line
			for (int i = 0; i < cellsConnected.Length; ++i) {
				if (cellsConnected [i].cell == secondCell) {
					// check it
					checkCell (i);
					break;
				}
			}
		}
	}

	void killNotConnectedCells ()
	{
		for (int i = 0; i < cellsConnected.Length; ++i) {
			// cell exists
			if (cellsConnected [i].cell != null) {
				if (cellsConnected [i].isConnected) {
					setCellConnection (cellsConnected [i].cell, true);
				} else {
					// cell is not connected to main cell
					setCellConnection (cellsConnected [i].cell, false);
				}
			}
		}
	}

	// ----------------- BUILD CELL FUNCTIONS -----------------------------

	public void UpdateNumberOfCell (GameObject cell, int delta)
	{
		if (cell.name == "mainCell") {
			NumMain += delta;
		}
		if (cell.name == "emptyCell") {
			NumEmpty += delta;
		}
		if (cell.name == "eatingCell") {
			NumEating += delta;
		}
		if (cell.name == "storeCell") {
			NumStore += delta;
		}
		if (cell.name == "movingCell") {
			NumMoving += delta;
		}
		if (cell.name == "o2Cell") {
			NumO2 += delta;
		}
	}

	public bool CanBuildCell ()
	{
		return canBuildCell (NewCellType);
	}

	bool canBuildCell (GameObject newCell)
	{
		if (newCell == SelectTypeOfNewCell.typeCell.MainCell) {
			return NumMain > 0;
		}
		if (newCell == SelectTypeOfNewCell.typeCell.EmptyCell) {
			return NumEmpty > 0;
		}
		if (newCell == SelectTypeOfNewCell.typeCell.EatingCell) {
			return NumEating > 0;
		}
		if (newCell == SelectTypeOfNewCell.typeCell.StoreCell) {
			return NumStore > 0;
		}
		if (newCell == SelectTypeOfNewCell.typeCell.MovingCell) {
			return NumMoving > 0;
		}
		if (newCell == SelectTypeOfNewCell.typeCell.O2Cell) {
			return NumO2 > 0;
		}

		return false;
	}

	public bool CanCellBeRemoved (GameObject cell)
	{
		if (cell.tag != "cell") {
			return false;
		}
		
		return (cell.GetComponent<CellCreation> () as CellCreation).CanBeRemoved;
	}

	public bool IsCellConnected (GameObject cell)
	{
		if (cell.tag != "cell") {
			return false;
		}

		return (cell.GetComponent<CellCreation> () as CellCreation).IsCellConnected;
	}

	public bool WasCellRemoved (GameObject cell)
	{
		if (cell.tag != "cell") {
			return false;
		}
		
		return (cell.GetComponent<CellCreation> () as CellCreation).WasRemoved;
	}

	public void SetCellRemoved (GameObject cell, bool wasCellRemoved)
	{		
		if (cell.tag != "cell") {
			return;
		}
		
		(cell.GetComponent<CellCreation> () as CellCreation).WasRemoved = wasCellRemoved;
	}

	void setCellConnection (GameObject cell, bool isConnected)
	{
		if (cell.tag != "cell") {
			return;
		}
		
		(cell.GetComponent<CellCreation> () as CellCreation).IsCellConnected = isConnected;
	}
}
