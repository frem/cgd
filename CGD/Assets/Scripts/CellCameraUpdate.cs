using UnityEngine;
using System.Collections;

public class CellCameraUpdate : MonoBehaviour
{
	void FixedUpdate ()
	{
		if (PlayerInfo.playerInfo.IsCellConnected (gameObject)) {
			// update rectangle containing cells
			PlayerInfo.playerInfo.CellsRectMax = Vector2.Max (PlayerInfo.playerInfo.CellsRectMax,
			                                                  transform.position);
			PlayerInfo.playerInfo.CellsRectMin = Vector2.Min (PlayerInfo.playerInfo.CellsRectMin,
			                                                  transform.position);
		}
	}
}
