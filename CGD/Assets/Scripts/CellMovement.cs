﻿using UnityEngine;
using System.Collections;

public class CellMovement : MonoBehaviour
{
	// force to push around (1-5)
	public float Force = 5f;

	// to make miliseconds into seconds
	public const float ForceMultiplier = 1000f;

	// variable for energy cost for movement in percent (0f : no cost - 1f : full cost)
	static float MovementCost = 0.25f;
	
	// Update is called once per frame
	void Update ()
	{
		if ((Input.GetMouseButton (0)) &&
			(gameObject.tag == "cell") &&
			(!PlayerInfo.playerInfo.BuildingMode)) {
	
			Plane plane = new Plane (Vector3.forward, Vector3.zero);
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			float hitDistance = 0f;
			if (plane.Raycast (ray, out hitDistance)) {
				// point on screen with mouse cursor
				Vector3 point = ray.GetPoint (hitDistance);
				// normalized vector of direction
				Vector3 f = (point - transform.position);
				f.Normalize ();
				f *= (Force * Time.deltaTime * ForceMultiplier);

				//spend energy
				PlayerInfo.playerInfo.Energy -= Time.deltaTime * Force * MovementCost;
				gameObject.GetComponent<Rigidbody2D>().AddForce (f);
			}
		}
	}
}
