using UnityEngine;
using System.Collections;

public class CellFinish : MonoBehaviour
{
	void OnTriggerStay2D (Collider2D coll)
	{
		if (coll.gameObject.tag != "finish") {
			return;
		}

		if (! PlayerInfo.playerInfo.Finish) {
			AudioCenter.audioCenter.PlayWin();
			PlayerInfo.playerInfo.Finish = true;
		}
	}
}
