using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	public float MinRectSize = 4f;
	public float SpeedZoom = 10f;
	public float Zoom = 50f;
	public float FinnishZoom = 15f;

	//moving average
	public int N = 10;

	Vector2[] oldCellsRectMax, oldCellsRectMin;
	Vector2 cellsRectMax, cellsRectMin, oldTotalCellsRectMax, oldTotalCellsRectMin;
	int i;

	void Start ()
	{
		oldCellsRectMax = new Vector2[N];
		oldCellsRectMin = new Vector2[N];
	}

	void FixedUpdate ()
	{
		simpleCameraFollow ();

		PlayerInfo.playerInfo.CellsRectMax = new Vector2 (float.MinValue, float.MinValue);
		PlayerInfo.playerInfo.CellsRectMin = new Vector2 (float.MaxValue, float.MaxValue);
	}

	void cameraFollow ()
	{
		cellsRectMax = PlayerInfo.playerInfo.CellsRectMax +
			Vector2.Max (Vector2.zero, (PlayerInfo.playerInfo.CellsRectMax - oldTotalCellsRectMax / N) * Time.fixedTime * SpeedZoom);
		cellsRectMin = PlayerInfo.playerInfo.CellsRectMin +
			Vector2.Min (Vector2.zero, (PlayerInfo.playerInfo.CellsRectMin - oldTotalCellsRectMin / N) * Time.fixedTime * SpeedZoom);

		Rect rect = new Rect (cellsRectMin.x,
		                     cellsRectMin.y,
		                     cellsRectMax.x - cellsRectMin.x,
		                     cellsRectMax.y - cellsRectMin.y);

		float z = -10f;
		if (Camera.main.orthographic) {
			float orthographicSize = Mathf.Max (MinRectSize,
			                                    Mathf.Max (rect.width /*/ Camera.main.aspect */, rect.height));
			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize,
			                                           orthographicSize,
			                                           Time.fixedTime);
		} else {
			// z = (A * sin(a) )/ sin(b) = ((rectSize * sin(30)) / sin(60))
			z = - (Mathf.Max (MinRectSize, Mathf.Max (rect.width /*/ Camera.main.aspect*/, rect.height)) / 0.50f) * 0.8656f;
		}
		transform.position = Vector3.Lerp (transform.position,
		                                   new Vector3 (rect.center.x, rect.center.y, z),
		                                   Time.fixedTime);

		// Update totalcellsRect for average
		int lastI = (i + 1) % N;
	
		oldCellsRectMax [i] = PlayerInfo.playerInfo.CellsRectMax;
		oldCellsRectMin [i] = PlayerInfo.playerInfo.CellsRectMin;

		oldTotalCellsRectMax = oldTotalCellsRectMax - oldCellsRectMax [lastI] + oldCellsRectMax [i];
		oldTotalCellsRectMin = oldTotalCellsRectMin - oldCellsRectMin [lastI] + oldCellsRectMin [i];

		i = lastI;
	}

	void simpleCameraFollow ()
	{
		Rect rect = new Rect (PlayerInfo.playerInfo.CellsRectMin.x,
		                     PlayerInfo.playerInfo.CellsRectMin.y,
		                     PlayerInfo.playerInfo.CellsRectMax.x - PlayerInfo.playerInfo.CellsRectMin.x,
		                     PlayerInfo.playerInfo.CellsRectMax.y - PlayerInfo.playerInfo.CellsRectMin.y);

		Camera.main.orthographicSize = 10f;

		float z = Zoom - Mathf.Min(50f, GameObject.FindGameObjectsWithTag("cell").Length);
		if (PlayerInfo.playerInfo.Finish) {
			z = FinnishZoom;
		}

		z = Mathf.Lerp (transform.position.z, z, Time.deltaTime);

		transform.position = new Vector3 (rect.center.x, rect.center.y, z);
	}
}
