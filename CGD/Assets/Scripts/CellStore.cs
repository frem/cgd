﻿using UnityEngine;
using System.Collections;

public class CellStore : MonoBehaviour
{
	void Awake ()
	{
		if (gameObject.tag == "cell")
			PlayerInfo.playerInfo.MaxEnergy += PlayerInfo.CellEnergyStoreCapacity;
	}

	// when cell is destroyed
	void OnDisable ()
	{
		if (gameObject.tag == "cell")
			PlayerInfo.playerInfo.MaxEnergy -= PlayerInfo.CellEnergyStoreCapacity;
	}
}
