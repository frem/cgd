using UnityEngine;
using System.Collections;

public class CellRemove : MonoBehaviour
{
	void OnMouseDown ()
	{
		// remove cell (building mode)
		if ((PlayerInfo.playerInfo.NewCellType == null)
			&& (gameObject.tag == "cell")
			&& (PlayerInfo.playerInfo.BuildingMode)
			&& PlayerInfo.playerInfo.CanCellBeRemoved (gameObject)) {

			PlayerInfo.playerInfo.SetCellRemoved(gameObject, true);

			(gameObject.GetComponent<CellHealth> () as CellHealth).Health = 0f;
			PlayerInfo.playerInfo.UpdateNumberOfCell (gameObject, +1);
			AudioCenter.audioCenter.PlayCellDeleted ();
		}
	}
}
