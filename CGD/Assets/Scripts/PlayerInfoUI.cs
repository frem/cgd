using UnityEngine;
using System.Collections;

public class PlayerInfoUI : MonoBehaviour
{
	public GUISkin EnergyColor;
	public GUISkin EnergyDying;
	public GUISkin EnergyBackground;

	public GameObject DyingBackground;

	public Texture2D FinishTexture;
	public Texture2D GameOverTexture;

	float eneBarWiB = 10f; // width of energy bar - big main down
	float eneBarWiS = 10f; // width of energy bars - small onces up

	void OnGUI ()
	{
		if (DyingBackground != null) {
			(DyingBackground.GetComponent<SpriteRenderer> ()).enabled = PlayerInfo.playerInfo.Dying;
		}

		if (PlayerInfo.playerInfo.Finish) {
			GUI.Label (new Rect ((Screen.width / 2) - 400f, 0f, 800f, 298f),
			           new GUIContent (FinishTexture));
		}

		if (PlayerInfo.playerInfo.Dead) {
			GUI.Label (new Rect ((Screen.width / 2) - 400f, 0f, 800f, 298f),
			           new GUIContent (GameOverTexture));
		}

		if ((Time.timeScale == 0f) || PlayerInfo.playerInfo.Finish) {
			return;
		}

		float maxEnergy = PlayerInfo.playerInfo.MaxEnergy;
		float energy = Mathf.Min (maxEnergy - 0.001f, PlayerInfo.playerInfo.Energy - 0.001f);

		// energy bar label background
		int width = Screen.width;
		int height = Screen.height;

		GUI.skin = EnergyBackground;
		// bottom big main bar
		GUI.Label (new Rect (5f, height - (11f + eneBarWiB), width - 10f, (6f + eneBarWiB)),
		           ""); // height: 5 + 3 + eneBarWiB + 3
		// small bars above him
		int bars = 0;
		bars = (int)(maxEnergy / PlayerInfo.CellEnergyStoreCapacity);
		float x = 5f;
		float y = height - (20f + eneBarWiB + eneBarWiS); // 5 + (3 + eneBarWiB + 3) + 5 + (2 + eneBarWiS + 2)
		for (int i = 1; i < bars; ++i) {
			GUI.Label (new Rect (x, y, 40f, (4f + eneBarWiS)), "");
			x += 45f; // 40 + 5
		}

		// energy bar
		if (PlayerInfo.playerInfo.Dying) {
			GUI.skin = EnergyDying;
			float energyOverZero = energy + PlayerInfo.CellEnergyStoreCapacity;
			float percent = energyOverZero / PlayerInfo.CellEnergyStoreCapacity;
			float newMainEnergy = percent * (width - 16f); // 5 + 3 + 3 + 5
			GUI.Label (new Rect (8f, height - (8f + eneBarWiB), newMainEnergy, eneBarWiB),
			           ""); // 5 + 3; height: 5 + 3 + eneBarWiB
		} else {
			GUI.skin = EnergyColor;
			float energyHundred = energy % PlayerInfo.CellEnergyStoreCapacity;
			float percent = energyHundred / PlayerInfo.CellEnergyStoreCapacity;
			percent = Mathf.Min (percent, 1.0f);
			float newMainEnergy = percent * (width - 16f); // 5 + 3 + 3 + 5
			GUI.Label (new Rect (8f, height - (8f + eneBarWiB), newMainEnergy, eneBarWiB),
			           ""); // 5 + 3; height: 5 + 3 + eneBarWiB

			x = 7f; // 5 + 2
			y = height - (18f + eneBarWiB + eneBarWiS); // 5 + (3 + eneBarWiB + 3) + 5 + (2 + eneBarWiS)
			for (int i = 1; i < bars; ++i) {
				if (energy > (i * PlayerInfo.CellEnergyStoreCapacity)) { // fill up if energy saver
					GUI.Label (new Rect (x, y, 36f, eneBarWiS), ""); // 40 - (2 + 2)
				}
				x += 45f; // 40 + 5
			}
		}
	}
}
