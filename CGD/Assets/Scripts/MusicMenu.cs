﻿using UnityEngine;
using System.Collections;

public class MusicMenu : MonoBehaviour
{
	bool pause = false;
	bool initialLoad = true;

	// singleton
	static MusicMenu _musicMenu;
	public static MusicMenu musicMenu {
		get { return _musicMenu; }
	}

	// Use this for initialization
	void Start ()
	{
		// singleton
		_musicMenu = this;
	}

	void Awake ()
	{
		// goes through scenes
		DontDestroyOnLoad (this);
	}

	// Update is called once per frame
	void Update ()
	{
		if (pause && (GetComponent<AudioSource>().volume < 0.1f)) {
			GetComponent<AudioSource>().Pause ();
			GetComponent<AudioSource>().volume = 0f;
		}
		if (pause && (GetComponent<AudioSource>().volume > 0f) && GetComponent<AudioSource>().isPlaying) {
			GetComponent<AudioSource>().volume -= 0.1f;
		}
		
		if ((!pause) && (!GetComponent<AudioSource>().isPlaying)) {
			GetComponent<AudioSource>().Play ();
		}
		if ((!pause) && GetComponent<AudioSource>().isPlaying && (GetComponent<AudioSource>().volume < 0.9f)) {
			GetComponent<AudioSource>().volume += 0.02f;
		} else {
			if ((!pause) && GetComponent<AudioSource>().isPlaying) {
				GetComponent<AudioSource>().volume = 1f;
			}
		}
		
		if (initialLoad) {
			initialLoad = false;
			Application.LoadLevel ("Welcome");
		}
	}

	public void StartMusic ()
	{
		pause = false;
	}

	public void StopMusic ()
	{
		pause = true;
	}
}
