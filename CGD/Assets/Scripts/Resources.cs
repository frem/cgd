﻿using UnityEngine;
using System.Collections;

public class Resources : MonoBehaviour
{
	// energy from particle
	public static float EnergyFromParticle = 40f;
	// energy from static resource per second
	public static float EnergyFromStatic = 20f;
	// force which applies on particle when eating cell around
	public static float PushResourceForce = 100f;
}
