using UnityEngine;
using System.Collections;

public class CellHealth : MonoBehaviour
{
	public bool DeadCell = false;

	// from 0 (dead) to maxHealth (full health)
	public float Health = 10f;
	// maxHealth - must be bigger then 0
	public static float MaxHealth = 100f;

	// min velocity to play sound
	static float MinMagnitudeToPlaySound = 5f;

	// Use this for initialization
	void Start ()
	{
		Health = MaxHealth;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!PlayerInfo.playerInfo.BuildingMode && (!PlayerInfo.playerInfo.IsCellConnected (gameObject))) {
			damageCell (PlayerInfo.HealingPerSecond + PlayerInfo.DamageIfNotConnected);
		}

		if (DeadCell) {
			// give info to organism
			PlayerInfo.playerInfo.NeedCheckConnection = true;

			// play sound if it was not removed by player
			if (! PlayerInfo.playerInfo.WasCellRemoved (gameObject)) {
				AudioCenter.audioCenter.PlayCellDestroyed ();
			}

			// kill the cell
			Destroy (transform.gameObject);
		}

		// healing
		if (!PlayerInfo.playerInfo.BuildingMode) {
			Health += PlayerInfo.HealingPerSecond * Time.deltaTime;
		}

		// more health
		if (Health > MaxHealth) {
			Health = MaxHealth;
		}

		// dead cell
		if (Health <= 0f) {
			killCell ();
		}
	}

	// physics update
	void FixedUpdate ()
	{
		// disable (red) Halo
		Behaviour h = (Behaviour)GetComponent ("Halo");
		if (h) {
			h.enabled = false;
		}
	}

	void OnTriggerStay2D (Collider2D coll)
	{
		if (coll.gameObject.tag != "trap") {
			return;
		}

		PlayerInfo.playerInfo.IsInTrap = true;

		damageCell (Traps.DamagePerSecond);
	}

	// hit
	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.gameObject.tag != "level") {
			return;
		}

		if (gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > MinMagnitudeToPlaySound) {
			AudioCenter.audioCenter.Play (AudioCenter.audioCenter.CellHitClip);
		}
	}

	void damageCell (float damage)
	{
		// enable (red) Halo
		Behaviour h = (Behaviour)GetComponent ("Halo");
		if (h) {
			h.enabled = true;
		}

		// do not kill main cell but decrease player energy
		if (gameObject.name == "MainCell") {
			PlayerInfo.playerInfo.Energy -= damage * Time.deltaTime;
		} else {
			Health -= damage * Time.deltaTime;
		}
	}

	void killCell ()
	{
		// do not destroy main cell
		if (gameObject.name == "MainCell") {
			return;
		}

		// kill springjoints connected to this cell
		GameObject[] cells = GameObject.FindGameObjectsWithTag ("cell") as GameObject[];
		foreach (GameObject cell in cells) {
			SpringJoint2D[] joints = cell.GetComponents<SpringJoint2D> ();
			foreach (SpringJoint2D joint in joints) {
				if (joint.connectedBody.transform.gameObject == transform.gameObject) {
					// joint connected to dead cell remove it
					Destroy (joint);
				}
			}
		}

		// kill springjoint from this cell
		SpringJoint2D[] ownJoints = transform.GetComponents<SpringJoint2D> ();
		foreach (SpringJoint2D joint in ownJoints) {
			Destroy (joint);
		}

		DeadCell = true;
	}
}
