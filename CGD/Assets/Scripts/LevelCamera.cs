﻿using UnityEngine;
using System.Collections;

public class LevelCamera : MonoBehaviour
{
	// level boundaries
	public float MinX = -5f;
	public float MaxX = 5f;
	public float MinY = -5f;
	public float MaxY = 5f;

	public bool OverLevel;

	float minDistance = -25f;
	float maxDistance = -50f;
	float scrollSpeed = 5f;

	// singleton
	static LevelCamera _levelCamera;
	public static LevelCamera levelCamera {
		get { return _levelCamera; }
	}

	void Start ()
	{
		OverLevel = false;
		MusicMenu.musicMenu.StartMusic (); // start menu music
	}

	void Awake ()
	{
		// singleton
		_levelCamera = this;
	}

	// Update is called once per frame
	void Update ()
	{
		if (OverLevel) {
			return;
		}
		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		
		// mouse scroll
		if (scroll != 0f) {
			Vector3 cameraPosition = transform.position;
			cameraPosition.z = cameraZ (cameraPosition.z + (scroll * scrollSpeed));
			transform.position = cameraPosition;
		}

		float horizontal = 0f;
		float vertical = 0f;
		// how fast scene goes with mouse
		float mouseMove = 1f;

		// left mouse button pressed and not creating cells
		if (Input.GetMouseButton (0)) {
			horizontal = Input.GetAxis ("Mouse X");
			vertical = Input.GetAxis ("Mouse Y");
			
			Vector3 cameraPosition = transform.position;
			cameraPosition.x = cameraPosition.x - (mouseMove * horizontal);
			cameraPosition.y = cameraPosition.y - (mouseMove * vertical);
			transform.position = checkBoundaries (cameraPosition);
		}
		
		// keyboard arrows and WSAD
		horizontal = Input.GetAxis ("Horizontal");
		vertical = Input.GetAxis ("Vertical");
		
		if (horizontal != 0f) {
			Vector3 cameraPosition = transform.position;
			cameraPosition.x = cameraPosition.x + horizontal;
			transform.position = checkBoundaries (cameraPosition);
		}
		
		if (vertical != 0f) {
			Vector3 cameraPosition = transform.position;
			cameraPosition.y = cameraPosition.y + vertical;
			transform.position = checkBoundaries (cameraPosition);
		}
	}

	float cameraZ (float z)
	{
		if (z > minDistance) {
			z = minDistance;
		}
		if (z < maxDistance) {
			z = maxDistance;
		}
		return z;
	}

	Vector3 checkBoundaries (Vector3 cameraPosition)
	{
		float x = cameraPosition.x;
		if (x > MaxX) {
			cameraPosition.x = MaxX;
		}
		if (x < MinX) {
			cameraPosition.x = MinX;
		}
		
		float y = cameraPosition.y;
		if (y > MaxY) {
			cameraPosition.y = MaxY;
		}
		if (y < MinY) {
			cameraPosition.y = MinY;
		}
		
		return cameraPosition;
	}
}
