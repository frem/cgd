﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
	public bool IsInGame = true;

	public GUISkin Skin;
	
	public GameObject PauseBackground;

	public static bool Showfps = false;

	public Color LowFPSColor = Color.red;
	public Color HighFPSColor = Color.green;
	
	public int LowFPS = 30;
	public int HighFPS = 50;

	public Color StatColor = Color.white;
	
	public Texture[] CreditIcons;
	
	public enum Page
	{
		None,
		Main,
		Options,
		Credits
	}
	
	Page currentPage;
	
	float fps;

	float savedTimeScale;

	string[] credits = {
		"Programmers\t\t\t: Peter Hlísta, Peter Hrinčár",
		"Graphics\t\t\t\t: Anna Pačesová",
		"Game design\t\t\t: Lucie Vinická",
		"Music and Sounds\t: Ján Ilavský",
		" ",
		"Copyright (c) 2014"} ;
	
	int toolbarInt = 0;
	string[] toolbarStrings = {"Audio", "Graphics", "Stats", "System"};

	bool endPauseGame = false;
	
	static bool IsDashboard ()
	{
		return (Application.platform == RuntimePlatform.OSXDashboardPlayer);
	}
	
	static bool IsBrowser ()
	{
		return ((Application.platform == RuntimePlatform.WindowsWebPlayer) ||
			(Application.platform == RuntimePlatform.OSXWebPlayer));
	}

	void Start ()
	{
		Time.timeScale = 1f;
		if (!IsInGame)
			pauseGame ();
	}

	void LateUpdate ()
	{
		if (Showfps) {
			fpsUpdate ();
		}

		if (IsInGame && (PlayerInfo.playerInfo.Finished || PlayerInfo.playerInfo.Dead)) {
			if (!endPauseGame) {
				endPauseGame = true;
				pauseGame ();
			}
			return;
		}
		
		if (Input.GetKeyDown ("escape")) {
			switch (currentPage) {
			case Page.None:
				pauseGame ();
				break;
				
			case Page.Main:
				if (IsInGame) {
					unPauseGame ();
				}
				break;
				
			default:
				currentPage = Page.Main;
				break;
			}
		}
	}
	
	void OnGUI ()
	{
		if (Skin != null) {
			GUI.skin = Skin;
		}
		showStatNums ();
		GUI.color = StatColor;

		if (isGamePaused ()) {
			switch (currentPage) {
			case Page.Main:
				mainPauseMenu ();
				break;
			case Page.Options:
				showToolbar ();
				break;
			case Page.Credits:
				showCredits ();
				break;
			}
		} else {
			if (GUI.Button (new Rect (Screen.width - 60f, 10f, 50f, 50f), "Pause")) {
				pauseGame ();
			}
			if (PlayerInfo.playerInfo.InfoHelp) {
				GUI.color = Color.yellow;
			}
			if (GUI.Button (new Rect (Screen.width - 60f, 70f, 50f, 50f), "Help")) {
				PlayerInfo.playerInfo.InfoHelp = !PlayerInfo.playerInfo.InfoHelp;

				PlayerInfo.Info = PlayerInfo.playerInfo.InfoHelp;
			}
			GUI.color = Color.white;
			GUI.Label (new Rect (Screen.width - 70f, 130f, 60f, 20f), Application.loadedLevelName);
		}
	}

	void OnApplicationPause (bool pause)
	{
		if (isGamePaused ()) {
			//AudioListener.pause = true;
		}
	}
	
	void showToolbar ()
	{
		beginPageOnPossition ((Screen.width / 2) - 150f, 50f, 300, 300);
		toolbarInt = GUILayout.Toolbar (toolbarInt, toolbarStrings);
		switch (toolbarInt) {
		case 0:
			volumeControl ();
			break;
		case 1:
			qualities ();
			qualityControl ();
			break;
		case 2:
			statControl ();
			break;
		case 3:
			showDevice ();
			break;
		}
		endPage ();
	}
	
	void showCredits ()
	{
		beginPageOnPossition ((Screen.width / 2) - 150f, 50f, 300, 300);
		foreach (string credit in credits) {
			GUILayout.Label (credit);
		}
		if (CreditIcons != null) {
			foreach (Texture credit in CreditIcons) {
				GUILayout.Label (credit);
			}
		}
		endPage ();
	}
	
	void showBackButton ()
	{
		if (GUI.Button (new Rect ((Screen.width / 2) + 160f, 50f, 50f, 20f), "Back")) {
			currentPage = Page.Main;
		}
	}
	
	void showDevice ()
	{
		GUILayout.Label ("Unity player version " + Application.unityVersion);
		GUILayout.Label ("Graphics: " + SystemInfo.graphicsDeviceName + " " +
			SystemInfo.graphicsMemorySize + "MB\n" +
			SystemInfo.graphicsDeviceVersion + "\n" +
			SystemInfo.graphicsDeviceVendor);
		GUILayout.Label ("Shadows: " + SystemInfo.supportsShadows);
		GUILayout.Label ("Image Effects: " + SystemInfo.supportsImageEffects);
		GUILayout.Label ("Render Textures: " + SystemInfo.supportsRenderTextures);
	}
	
	void qualities ()
	{
		GUILayout.Label (QualitySettings.names [QualitySettings.GetQualityLevel ()]);
	}
	
	void qualityControl ()
	{
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Decrease")) {
			QualitySettings.DecreaseLevel ();
		}
		if (GUILayout.Button ("Increase")) {
			QualitySettings.IncreaseLevel ();
		}
		GUILayout.EndHorizontal ();
	}
	
	void volumeControl ()
	{
		GUILayout.Label ("Volume");

		PlayerInfo.Volume = GUILayout.HorizontalSlider (PlayerInfo.Volume, 0f, 1f);

		AudioListener.volume = PlayerInfo.Volume;
	}
	
	void statControl ()
	{
		GUILayout.BeginHorizontal ();
		Showfps = GUILayout.Toggle (Showfps, "FPS");
		GUILayout.EndHorizontal ();
	}
	
	void fpsUpdate ()
	{
		float delta = Time.smoothDeltaTime;
		if ((!isGamePaused ()) && (delta != 0f)) {
			fps = 1f / delta;
		}
	}
	
	void showStatNums ()
	{
		GUILayout.BeginArea (new Rect ((Screen.width - 100f), 10f, 100f, 200f));
		if (Showfps) {
			string fpsstring = fps.ToString ("#,##0 fps");
			GUI.color = Color.Lerp (LowFPSColor, HighFPSColor, (fps - LowFPS) / (HighFPS - LowFPS));
			GUILayout.Label (fpsstring);
		}
		GUILayout.EndArea ();
	}
	
	void beginPage (int width, int height)
	{
		GUILayout.BeginArea (new Rect ((Screen.width - width) / 2f, (Screen.height - height) / 2f, width, height));
	}

	void beginPageOnPossition (float x, float y, int width, int height)
	{
		GUILayout.BeginArea (new Rect (x, y, width, height));
	}
	
	void endPage ()
	{
		GUILayout.EndArea ();
		if (currentPage != Page.Main) {
			showBackButton ();
		}
	}

	void mainPauseMenu ()
	{
		if (IsInGame) {
			beginPage (120, 200);

			if ((!PlayerInfo.playerInfo.Dead) && (!PlayerInfo.playerInfo.Finished)) {
				if (GUILayout.Button ("Continue")) {
					unPauseGame ();			
				}
			}

			if (PlayerInfo.playerInfo.Finished) {
				if (GUILayout.Button ("Next Level")) {
					Application.LoadLevel (PlayerInfo.playerInfo.NextLevel);
					unPauseGame ();
				}
			}

			if (GUILayout.Button ("Restart")) {
				Application.LoadLevel (Application.loadedLevel);
				unPauseGame ();
			}

			
			GUILayout.Label ("");
			if (GUILayout.Button ("Options")) {
				currentPage = Page.Options;
			}
			if (GUILayout.Button ("Exit")) {
				Application.LoadLevel ("LevelSelect");
			}

		} else {
			beginPageOnPossition ((Screen.width / 2f) - 60f, 70f, 120, 200);
		
			if (GUILayout.Button ("Play")) {
				Application.LoadLevel ("LevelSelect");
			}

			GUILayout.Label ("");
			if (GUILayout.Button ("Options")) {
				currentPage = Page.Options;
			}
			if (GUILayout.Button ("Credits")) {
				currentPage = Page.Credits;
			}
		}

		endPage ();
	}
	
	void pauseGame ()
	{
		savedTimeScale = Time.timeScale;
		Time.timeScale = 0f;
		//AudioListener.pause = true;
		if (PauseBackground) {
			PauseBackground.GetComponent<SpriteRenderer> ().enabled = true;
		}
		currentPage = Page.Main;
	}
	
	void unPauseGame ()
	{
		Time.timeScale = savedTimeScale;
		//AudioListener.pause = false;
		if (PauseBackground) {
			PauseBackground.GetComponent<SpriteRenderer> ().enabled = false;
		}

		currentPage = Page.None;		
	}
	
	bool isGamePaused ()
	{
		return (Time.timeScale == 0f);
	}
}