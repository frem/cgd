using UnityEngine;
using System.Collections;

public class CellEat : MonoBehaviour
{
	// distance where particle is eatten
	public float MinDistance = 3f;

	static float HowLongPlaySoundAfterEatParticle = 1f; // (:

	float eatingSoundTimeRemaining;

	void FixedUpdate ()
	{
		if (eatingSoundTimeRemaining < 0) {
			eatingSoundTimeRemaining = 0;
		} else {
			eatingSoundTimeRemaining -= Time.deltaTime;
		}
	}

	void OnTriggerStay2D (Collider2D coll)
	{
		if (!PlayerInfo.playerInfo.IsCellConnected (gameObject)) {
			return;
		}

		if ((coll.gameObject.tag != "levelResource") && (coll.gameObject.tag != "particleResource")) {
			return;
		}

		float distance = Vector3.Distance (transform.position, coll.transform.position);

		Vector2 force = new Vector2 ();
		// force direction, inversely proportional to the square of the distance
		force = transform.position - coll.transform.position;
		//force.Normalize ();
		//force = (force * forceMass) / (((distance + transform.localScale.x) * (distance + transform.localScale.x)) + 0.1f) * Time.deltaTime;
		// square of distance (localScale.x == 0.1f so distance will not be too small)
		float divisor = (distance + transform.localScale.x) * (distance + transform.localScale.x);
		divisor += 1f; // so divisor will not be too small
		force = (force.normalized) / divisor;
		force *= CellMovement.ForceMultiplier;
		force *= Time.deltaTime; // take time into count

		// trigger was static resource
		if (coll.gameObject.tag == "levelResource") {
			// organisms is not dying anymore
			PlayerInfo.playerInfo.Energy = Mathf.Max (0f, PlayerInfo.playerInfo.Energy);

			float energyPerSecond = Resources.EnergyFromStatic;
			if (gameObject.tag == "cell") {
				PlayerInfo.playerInfo.Energy += energyPerSecond * Time.deltaTime;
				eatingSoundTimeRemaining = 0.1f;
			}
		}


		// trigger was particle
		if (coll.gameObject.tag == "particleResource") {
			if (distance < MinDistance) {
				// organisms is not dying anymore
				PlayerInfo.playerInfo.Energy = Mathf.Max (0f, PlayerInfo.playerInfo.Energy);

				// get energy from object
				float particleEnergy = Resources.EnergyFromParticle;
				if (gameObject.tag == "cell") {
					PlayerInfo.playerInfo.Energy += particleEnergy;
					eatingSoundTimeRemaining = HowLongPlaySoundAfterEatParticle;
				}

				// eat object
				GameObject resource = coll.gameObject;
				Destroy (resource);
				return;
			}

			// for debug line
			Debug.DrawLine (coll.transform.position,
			                new Vector3 (coll.transform.position.x + (10f * force.x),
				                         coll.transform.position.y + (10f * force.y),
				                         0f),
			                Color.green);

			float pushForce = Resources.PushResourceForce;
			if (coll.gameObject.GetComponent<Rigidbody2D>() != null) {
				// push particleResource to EatingCell
				coll.gameObject.GetComponent<Rigidbody2D>().AddForce (force * pushForce);
			}
		}
	}

	void LateUpdate ()
	{
		if (GetComponent<AudioSource>() == null) {
			return;
		}

		if (eatingSoundTimeRemaining > 0f) {
			GetComponent<AudioSource>().volume = Mathf.Lerp (GetComponent<AudioSource>().volume, 1f, Time.deltaTime * AudioCenter.SoundFadeSpeed);
		} else {
			GetComponent<AudioSource>().volume = Mathf.Lerp (GetComponent<AudioSource>().volume, 0f, Time.deltaTime * AudioCenter.SoundFadeSpeed);
		}
		
		if (GetComponent<AudioSource>().volume < 0.1f) {
			GetComponent<AudioSource>().Pause ();
			
		} else {
			if (!GetComponent<AudioSource>().isPlaying) {
				GetComponent<AudioSource>().Play ();
			}
		}
	}
}
